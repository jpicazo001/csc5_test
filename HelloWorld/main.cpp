/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 18, 2014, 11:44 AM
 */

#include <cstdlib>
#include <iostream> // Adds i/o to my program
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    cout << "HelloWorld" << endl; // Semicolon denotes end of statement
    return 0;
}

