/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 20, 2014, 10:33 AM
 */

#include <cstdlib> //cstdlib library not needed
#include <iostream> // is needed for output

// Gives the context of where my libraries are coming from
using namespace std;

/*
 * 
 */
//There is always and only one main
//Programs always start at main
// Programs execute from top to bottom, left to right
int main(int argc, char** argv) {
    
    // cout specifies output
    // endl creates a new line
    // << specifies stream operator
    // All statements end in a semicolon
    
    // using a programmer-defined identifier/variable
    // message is a variable
    // string is a data type
    // = Is an assignment operator
    // Assign right to left
    
    // string message = "Hello World";
    string message; // Variable declaration
    //message = "Hello World Lab Assignment 1"; // Variable initialization 
    
    // Prompt the user
    cout << "Please enter a message" << endl;
    
    cin >> message; // User input
    
    cout << "Your message is " << message << endl;
    
    
        //C++ ignores whitespace
    cout
    << "Jonathan Picazo, February 20";
    
    
    
    // If program hits return, it ran successfully
            return 0;
} // All my code is within curly braces

